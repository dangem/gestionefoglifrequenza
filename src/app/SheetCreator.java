package app;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Set;

import utilities.exceptions.OpenCVException;
import database.objects.Student;

public class SheetCreator {

    private String courseName;
    private String date;
    private Set<Student> students;

    private String textFilePath;
    private String imageFilePath;

    public SheetCreator(String courseName, String date, Set<Student> students) {
        this.courseName = courseName.replace(" ", "_");
        this.date = date.replace("/", "-").replace(" ", "_");
        this.students = students;

        this.imageFilePath = courseName + File.separator + courseName + "_"
                + this.date;
        this.textFilePath = imageFilePath + ".txt";
    }

    public void createSheet() throws IOException, InterruptedException,
            OpenCVException {
        createDataFile(courseName, date, students);
        startCreate();
    }

    private void startCreate() throws IOException, InterruptedException,
            OpenCVException {
        String command = GestioneFogliFrequenza.OPENCV_PATH + " "
                + textFilePath + " " + imageFilePath;
        Process process = Runtime.getRuntime().exec(command);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                process.getErrorStream()));
        process.waitFor();

        String opencvErrorMessage = reader.readLine();
        if (opencvErrorMessage != null) {
            throw new OpenCVException(opencvErrorMessage);
        }
    }

    private void createDataFile(String courseName, String date,
            Set<Student> students) throws IOException {
        File directory = new File(courseName);
        directory.mkdir();

        File file = new File(textFilePath);
        PrintWriter writer = new PrintWriter(file);

        writer.write(courseName + "\n");
        writer.write(date + "\n");
        writer.write(students.size() + "\n");

        for (Student student : students) {
            writer.write(student.toString() + "\n");
        }

        writer.close();
    }

    // public static void main(String[] args) throws IOException,
    // InterruptedException, OpenCVException {
    // // File f = new File("./file.txt");
    // // f.createNewFile();
    // Set<Student> students = new TreeSet<Student>();
    // students.add(new Student("1", "z", "a"));
    // students.add(new Student("2", "v", "b"));
    // students.add(new Student("3", "u", "c"));
    // students.add(new Student("4", "t", "d"));
    // students.add(new Student("5", "e", "s"));
    // students.add(new Student("6", "f", "r"));
    // students.add(new Student("7", "g", "q"));
    // students.add(new Student("8", "h", "p"));
    // students.add(new Student("9", "i", "o"));
    // // for (int i = 0; i < 14; i++)
    // // students.add(new Student(Integer.toString(i), Integer.toString(i*i),
    // Integer.toString(i*i*i)));
    //
    // SheetCreator creator = new SheetCreator("ingsw", "23-00-00", students);
    // creator.createSheet();
    // }

}
