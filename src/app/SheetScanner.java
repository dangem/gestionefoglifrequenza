package app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import utilities.exceptions.OpenCVException;

public class SheetScanner {

    private final static int NUMBER_OF_ITERATIONS = 6;

    private String templateFilePath;
    private String sheetFilePath;
    private String presencesFilePath;

    public SheetScanner(String templateFilePath, String sheetFilePath,
            String presencesFilePath) {
        this.templateFilePath = templateFilePath;
        this.sheetFilePath = sheetFilePath;
        this.presencesFilePath = presencesFilePath.replace("/", "-");
    }

    public List<Boolean> scanSheet() throws IOException, InterruptedException,
            OpenCVException {
        startScan();
        List<Boolean> presences = readResultsFromFile();
        cleanTempFiles();
        return presences;
    }

    private void startScan() throws IOException, InterruptedException,
            OpenCVException {
        String command = GestioneFogliFrequenza.OPENCV_PATH + " "
                + templateFilePath + " " + sheetFilePath + " "
                + presencesFilePath + " " + NUMBER_OF_ITERATIONS;
        Process process = Runtime.getRuntime().exec(command);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                process.getErrorStream()));
        process.waitFor();

        String opencvErrorMessage = reader.readLine();
        if (opencvErrorMessage != null) {
            throw new OpenCVException(opencvErrorMessage);
        }
    }

    private List<Boolean> readResultsFromFile() throws IOException {
        File resultsFile = new File(presencesFilePath);
        FileInputStream fileInputStream = new FileInputStream(resultsFile);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                fileInputStream));
        List<Boolean> presences = new ArrayList<Boolean>();

        String buffer = reader.readLine();
        while (buffer != null) {
            if (buffer.equals("presente"))
                presences.add(true);
            else if (buffer.equals("assente"))
                presences.add(false);
            buffer = reader.readLine();
        }

        fileInputStream.close();
        reader.close();

        return presences;
    }

    private void cleanTempFiles() {
        File file = new File(presencesFilePath);
        file.delete();
    }
}
