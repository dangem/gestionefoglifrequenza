package app;

import gui.GUIManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.SwingUtilities;

import database.Database;

public class GestioneFogliFrequenza {

    public final static String OPENCV_PATH = ".." + File.separator + ".."
            + File.separator + "C++" + File.separator + "Release"
            + File.separator + "OpenCVRecognizer.exe";

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                GestioneFogliFrequenza app = new GestioneFogliFrequenza();
                app.initDatabase();
                app.initGUI();
            }
        });
    }

    private void initGUI() {
        GUIManager.get().showMainFrame();
    }

    private void initDatabase() {
        try {
            Database.get().load();
        } catch (FileNotFoundException fnfe) {
            GUIManager.get().databaseNotFoundDialog();
            try {
                Database.get().initNew();
            } catch (IOException ioe) {
                GUIManager.get().showIOErrorDialog();
                System.exit(1);
            }
        }
    }
}
