package database;

public interface DatabaseListener {

    public void databaseModified();
}
