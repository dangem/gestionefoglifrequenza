package database.objects;

public class PresenceCounter {

    private int value;

    public PresenceCounter() {
        this.setValue(0);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void increase() {
        value += 1;
    }

    public void decrease() {
        if (value - 1 >= 0)
            value -= 1;
    }

}
