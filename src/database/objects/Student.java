package database.objects;

import java.util.HashSet;
import java.util.Set;

import database.Database;

/**
 * Memorizza le informazioni riguardanti uno studente all'interno del database.
 * <p>
 * I metodi che modificano i campi della classe sono volutamente resi protected
 * in quanto si vorrebbe rendere utilizzabile questa classe solo dalle classi
 * Manager contenute in questo stesso package, in quanto � l� che vengono
 * effettuati i controlli sulla validit� dell'input.
 */
public class Student implements Comparable<Student> {

    private String studentID;
    private String surname;
    private String name;

    /**
     * I corsi ai quali lo studente � iscritto. Rende pi� efficiente la modifica
     * dei dati dello studente attraverso il database e tramite l'accesso alla
     * sua size() permette di capire se uno studente non fa pi� parte di nessun
     * corso.
     */
    private Set<Course> attendedCourses;

    /**
     * Costruttore protected in quanto si vorrebbe rendere istanziabile un nuovo
     * studente solo dall'interno del database.
     */
    public Student(String studentID, String surname, String name) {
        this.studentID = studentID;
        this.surname = surname;
        this.name = name;

        this.attendedCourses = new HashSet<>();
    }

    protected void setName(String name) {
        this.name = name;
    }

    protected void setSurname(String surname) {
        this.surname = surname;
    }

    protected void setID(String studentID) {
        this.studentID = studentID;
    }

    protected boolean addAttendedCourse(Course course) {
        return attendedCourses.add(course);
    }

    protected boolean removeAttendedCourse(Course course) {
        boolean removed = attendedCourses.remove(course);

        if (getCoursesCount() <= 0) {
            Database.get().removeStudent(this);
        }

        return removed;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getID() {
        return studentID;
    }

    public int getCoursesCount() {
        return attendedCourses.size();
    }

    public Set<Course> getAttendedCourses() {
        return attendedCourses;
    }

    /**
     * Gli studenti vengono comparati per cognome. Se i cognomi sono uguali
     * vengono comparati per ID.
     */
    @Override
    public int compareTo(Student otherStudent) {
        int comparison = this.surname.compareTo(otherStudent.surname);

        if (comparison == 0) {
            return this.studentID.compareTo(otherStudent.studentID);
        } else {
            return comparison;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result
                + ((studentID == null) ? 0 : studentID.hashCode());
        result = prime * result + ((surname == null) ? 0 : surname.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Student other = (Student) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (studentID == null) {
            if (other.studentID != null)
                return false;
        } else if (!studentID.equals(other.studentID))
            return false;
        if (surname == null) {
            if (other.surname != null)
                return false;
        } else if (!surname.equals(other.surname))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return this.studentID + " - " + this.surname + " " + this.name;
    }
}
