package database.objects;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import utilities.exceptions.LessonCheckingException;

/**
 * Memorizza le informazioni riguardanti una lezione associata ad un corso
 * all'interno del database.
 * <p>
 * I metodi che modificano i campi della classe sono volutamente resi protected
 * in quanto si vorrebbe rendere utilizzabile questa classe solo dalle classi
 * Manager contenute in questo stesso package, in quanto � l� che vengono
 * effettuati i controlli sulla validit� dell'input.
 */
public class Lesson implements Comparable<Lesson> {

    private String lessonID;
    private String date;

    private Map<Student, Boolean> presences;

    /**
     * Una lezione � "checked" quando il suo relativo foglio di frequenza �
     * stato generato. Per evitare inconsistenze tra i dati stampati sul foglio
     * e quelli memorizzati sul database, una lezione "checked" non pu� essere
     * modificata. Se si desidera modificare una lezione "checked" bisogner�
     * reimpostare tale valore a <b>false</b> e generare nuovamente il foglio di
     * frequenza prima di analizzarlo.
     */
    private boolean checked;

    /**
     * Costruttore protected in quanto si vorrebbe rendere istanziabile una
     * nuova lezione solo dall'interno del database.
     */
    protected Lesson(Course course, String date, Set<Student> students) {
        this.lessonID = date;
        this.date = date;

        presences = new TreeMap<Student, Boolean>();

        for (Student student : students) {
            presences.put(student, new Boolean(false));
        }

        checked = false;
    }

    /**
     * Assegna ad ogni studente della lezione una presenza o un'assenza a
     * seconda dei dati contenuti nella lista ricevuta come parametro.
     * 
     * @param presencesList
     *            una lista di Boolean che per ogni studente memorizza se �
     *            stato presente (<b>true</b>) o assente (<b>false</b>) a questa
     *            lezione. L'ordine dei valori nella lista � significativo in
     *            quanto gli studenti della lezione sono memorizzati nella Map
     *            <code>presences</code> in ordine alfabetico, e si accede ad
     *            essi in tale ordine tramite un iteratore.
     * @returns <b>true</b> se il checking � andato a buon fine, <b>false</b> se
     *          la lezione era gi� checked.
     * @throws LessonCheckingException
     */
    public boolean check(List<Boolean> presencesList)
            throws LessonCheckingException {

        if (isChecked()) {
            return false;
        }

        if (presencesList.size() != presences.size()) {
            throw new LessonCheckingException();
        }

        Iterator<Boolean> it = presencesList.iterator();
        for (Student student : getStudents()) {
            if (it.next().booleanValue())
                presences.put(student, new Boolean(true));
        }

        checked = true;
        return true;
    }

    public boolean isChecked() {
        return checked;
    }

    public String getID() {
        return lessonID;
    }

    public String getDate() {
        return date;
    }

    public Set<Student> getStudents() {
        return presences.keySet();
    }

    public boolean isPresent(Student student) {
        return presences.get(student);
    }

    public boolean editPresence(Student student) {
        Boolean presence = presences.get(student);

        if (presence.booleanValue() == true) {
            presences.put(student, new Boolean(false));
            return false;
        } else {
            presences.put(student, new Boolean(true));
            return true;
        }
    }

    protected void rename(String newCourseName) {
        this.lessonID = newCourseName + " " + date;
    }

    protected Iterator<Entry<Student, Boolean>> getStudentsIterator() {
        return presences.entrySet().iterator();
    }

    @Override
    public int compareTo(Lesson o) {
        return this.lessonID.compareTo(o.lessonID);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((lessonID == null) ? 0 : lessonID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Lesson other = (Lesson) obj;
        if (lessonID == null) {
            if (other.lessonID != null)
                return false;
        } else if (!lessonID.equals(other.lessonID))
            return false;
        return true;
    }

    @Override
    public String toString() {
        if (isChecked()) {
            return lessonID + " - Analizzata";
        }

        return lessonID + " - DA ANALIZZARE";
    }
}
