package database.objects;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import utilities.exceptions.LessonCheckingException;

/**
 * Memorizza le informazioni riguardanti un corso all'interno del database.
 * <p>
 * I metodi che modificano i campi della classe sono volutamente resi protected
 * in quanto si vorrebbe rendere utilizzabile questa classe solo dalle classi
 * Manager contenute in questo stesso package, in quanto � l� che vengono
 * effettuati i controlli sulla validit� dell'input.
 */
public class Course implements Comparable<Course> {

    private String courseName;

    /**
     * Mappa che associa ad ogni studente (chiave) un contatore delle sue
     * presenze maturate per questo corso.
     */
    private Map<Student, PresenceCounter> students;

    /** Elenco delle lezioni del corso per nome. */
    private Map<String, Lesson> lessons;

    private Set<Lesson> lessonsByID;

    /**
     * Costruttore protected in quanto si vorrebbe rendere istanziabile un nuovo
     * corso solo dall'interno del database.
     */
    protected Course(String courseName) {
        this.courseName = courseName;

        this.students = new TreeMap<Student, PresenceCounter>();
        this.lessons = new HashMap<String, Lesson>();
        this.lessonsByID = new TreeSet<Lesson>();
    }

    /**
     * Aggiunge uno studente al corso.
     * 
     * @param student
     *            Lo studente da aggiungere.
     * @return <b>true</b> se l'aggiunta � andata a buon fine, <b>false</b> se
     *         lo studente fa gi� parte del corso.
     */
    protected boolean addStudent(Student student) {
        if (students.containsKey(student)) {
            return false;
        }

        students.put(student, new PresenceCounter());
        student.addAttendedCourse(this);
        return true;
    }

    /**
     * Rimuove uno studente dal corso.
     * 
     * @param student
     *            Lo studente da rimuovere.
     * @return <b>true</b> se la rimozione � andata a buon fine, <b>false</b> se
     *         lo studente non fa parte del corso.
     */
    protected boolean removeStudent(Student student) {
        if (!students.containsKey(student)) {
            return false;
        }

        students.remove(student);
        student.removeAttendedCourse(this);
        return true;
    }

    /** Restituisce un Set degli studenti iscritti al corso. */
    public Set<Student> getStudents() {
        return students.keySet();
    }

    /**
     * Aggiunge una lezione al corso.
     * 
     * @param lesson
     *            La lezione da aggiungere.
     * @return <b>true</b> se l'inserimento � andato a buon fine, <b>false</b>
     *         se esiste gi� una lezione con lo stesso ID appartenente al corso.
     */
    protected boolean createLesson(String date) {
        Lesson lesson = new Lesson(this, date, getStudents());

        if (lessons.containsKey(lesson)) {
            return false;
        }

        lessons.put(lesson.getID(), lesson);
        lessonsByID.add(lesson);
        return true;
    }

    protected boolean deleteLesson(Lesson lesson) {
        if (!lessons.containsKey(lesson.getID())) {
            return false;
        }

        for (Student student : lesson.getStudents()) {
            if (lesson.isPresent(student))
                students.get(student).decrease();
        }

        lessonsByID.remove(lesson);
        lessons.remove(lesson.getID());

        return true;
    }

    public String getName() {
        return courseName;
    }

    public Set<Lesson> getLessons() {
        return lessonsByID;
    }

    /**
     * Restituisce una lezione dato come argomento il suo ID, oppure <b>null</b>
     * se non esiste nessuna lezione con quell'ID.
     */
    public Lesson getLessonByID(String lessonID) {
        return lessons.get(lessonID);
    }

    void setName(String courseName) {
        this.courseName = courseName;
    }

    protected boolean checkLesson(Lesson lesson, List<Boolean> presencesList)
            throws LessonCheckingException {
        if (!lessons.containsKey(lesson.getID())) {
            return false;
        }

        if (!lesson.check(presencesList)) {
            return false;
        }

        for (Student student : lesson.getStudents()) {
            if (lesson.isPresent(student)) {
                students.get(student).increase();
            }
        }

        return true;
    }

    protected void editPresence(Lesson lesson, Student student) {
        if (!lesson.isChecked()) {
            return;
        }

        if (lesson.editPresence(student)) {
            students.get(student).increase();
        } else {
            students.get(student).decrease();
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((courseName == null) ? 0 : courseName.hashCode());
        return result;
    }

    /** Due corsi sono uguali se hanno lo stesso nome. */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Course other = (Course) obj;
        if (courseName == null) {
            if (other.courseName != null)
                return false;
        } else if (!courseName.equals(other.courseName))
            return false;
        return true;
    }

    /** I corsi vengono comparati in ordine alfabetico. */
    @Override
    public int compareTo(Course anotherCourse) {
        return this.courseName.compareTo(anotherCourse.courseName);
    }

    @Override
    public String toString() {
        return courseName;
    }

    public int getPresences(Student student) {
        if (!students.containsKey(student)) {
            return -1;
        }

        return students.get(student).getValue();
    }
}