package database.objects;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import utilities.exceptions.LessonCheckingException;

public class CoursesManager {

    /**
     * Map che associa ad ogni corso il suo nome, per ottimizzare l'accesso
     * casuale ai corsi del database.
     */
    Map<String, Course> courses;

    /**
     * Viene utilizzato un TreeSet apposito per tenere un Set dei corsi ordinato
     * per cognome, che viene tenuto consistente con i dati di
     * <code>coursesByName</code> tramite il metodo <code>addCourse</code>. �
     * utile alla GUI per la rappresentazione dei dati.
     */
    Set<Course> coursesByName;

    public CoursesManager() {
        courses = new TreeMap<String, Course>();
        coursesByName = new TreeSet<Course>();
    }

    /**
     * Inserisce un nuovo corso nel database se non � gi� presente un altro
     * corso con lo stesso nome.
     * 
     * @param courseName
     *            il nome del corso da inserire.
     * @return <b>true</b> se l'inserimento � andato a buon fine, <b>false</b>
     *         se quel corso � gi� presente nel database.
     */
    public boolean createCourse(String courseName) {
        if (courses.containsKey(courseName)) {
            return false;
        }

        Course course = new Course(courseName);

        courses.put(courseName, course);
        coursesByName.add(course);
        return true;
    }

    /**
     * Rimuove un corso dal database.
     * 
     * @param course
     *            il corso da rimuovere.
     * @return <b>true</b> se la rimozione � andata a buon fine, <b>false</b> se
     *         quel corso non � presente nel database.
     */
    public boolean deleteCourse(Course course) {
        String courseName = course.getName();

        if (!courses.containsKey(courseName)) {
            return false;
        }

        Set<Student> courseStudents = new HashSet<Student>();

        for (Student student : course.getStudents()) {
            courseStudents.add(student);
        }

        for (Student courseStudent : courseStudents) {
            course.removeStudent(courseStudent);
        }

        coursesByName.remove(course);
        courses.remove(courseName);
        return true;
    }

    /**
     * Rinomina un corso.
     * 
     * @param course
     *            il corso da rinominare.
     * @param newCourseName
     *            il nuovo nome da assegnare al corso.
     * @return <b>true</b> se la ridenominazione � andata a buon fine,
     *         <b>false</b> se il nuovo nome scelto appartiene gi� ad un altro
     *         corso del database.
     */
    public boolean renameCourse(Course course, String newCourseName) {
        if (courses.containsKey(newCourseName)) {
            return false;
        }

        courses.remove(course.getName());

        course.setName(newCourseName);
        courses.put(newCourseName, course);
        return true;
    }

    public Course getCourseByName(String courseName) {
        return courses.get(courseName);
    }

    /** Restituisce un Set dei nomi dei corsi che si trovano nel database. */
    public Set<Course> getCourses() {
        return coursesByName;
    }

    /**
     * Aggiunge uno studente ad un corso, e imposta il valore totale delle sue
     * presenze per quel corso.
     * 
     * @param student
     *            Lo studente da aggiungere.
     * @param course
     *            Il corso al quale lo studente deve essere aggiunto.
     * @return <b>true</b> se l'inserimento � andato a buon fine, <b>false</b>
     *         se lo studente appartiene gi� al corso.
     */
    public boolean addStudentToCourse(Student student, Course course) {
        return course.addStudent(student);
    }

    public boolean removeStudentFromCourse(Student student, Course course) {
        return course.removeStudent(student);
    }

    public boolean createLesson(Course course, String date) {
        return course.createLesson(date);
    }

    public boolean deleteLesson(Course course, Lesson lesson) {
        return course.deleteLesson(lesson);
    }

    public boolean checkLesson(Course course, Lesson lesson,
            List<Boolean> presencesList) throws LessonCheckingException {
        return course.checkLesson(lesson, presencesList);
    }

    public void editPresence(Course course, Lesson lesson, Student student) {
        course.editPresence(lesson, student);
    }
}