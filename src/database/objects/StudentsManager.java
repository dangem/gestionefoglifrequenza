package database.objects;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class StudentsManager {

    /**
     * Map che associa ad ogni corso il suo ID, per ottimizzare l'accesso
     * casuale agli studenti del database tramite una chiave univoca.
     */
    Map<String, Student> students;

    /**
     * Viene utilizzato un TreeSet apposito per tenere un Set di studenti
     * ordinato per cognome, che viene tenuto consistente con i dati di
     * <code>studentsByID</code> tramite il metodo <code>addStudent</code>. �
     * utile alla GUI per la rappresentazione dei dati.
     */
    Set<Student> studentsBySurname;

    public StudentsManager() {
        students = new TreeMap<String, Student>();
        studentsBySurname = new TreeSet<Student>();
    }

    /**
     * Inserisce uno studente nel database se non ne � gi� presente un altro con
     * lo stesso ID.
     * 
     * @param studentID
     *            l'ID dello studente da inserire.
     * @param surname
     *            il cognome dello studente da inserire.
     * @param name
     *            il nome dello studente da inserire.
     * @return <b>true</b> se l'inserimento � andato a buon fine, <b>false</b>
     *         se quello studente � gi� presente nel database.
     */
    public boolean createStudent(String studentID, String surname, String name) {
        if (students.containsKey(studentID)) {
            return false;
        }

        Student student = new Student(studentID, surname, name);

        students.put(studentID, student);
        studentsBySurname.add(student);
        return true;
    }

    /**
     * Rimuove uno studente dal database.
     * 
     * @param student
     *            lo studente da rimuovere.
     * @return <b>true</b> se la rimozione � andata a buon fine, <b>false</b> se
     *         quello studente non � presente nel database.
     */
    public boolean removeStudent(Student student) {
        String studentID = student.getID();

        if (!students.containsKey(studentID)) {
            return false;
        }

        studentsBySurname.remove(student);
        students.remove(studentID);
        return true;
    }

    /**
     * Modifica uno studente, sostituendo al vecchio studente un nuovo studente
     * con i dati di quello vecchio opportunamente modificati.
     * 
     * @param oldStudent
     *            lo studente da modificare.
     * @param newID
     *            il nuovo ID da assegnare allo studente.
     * @param newSurname
     *            il nuovo cognome da assegnare allo studente.
     * @param newName
     *            il nuovo nome da assegnare allo studente.
     * @return <b>true</b> se la modifica � andata a buon fine, <b>false</b>
     *         altrimenti. La modifica pu� fallire se il nuovo ID da assegnare
     *         corrisponde gi� ad un altro studente, o se l'ID dello studente da
     *         modificare non esiste nel database.
     */
    public boolean editStudent(Student oldStudent, String newID,
            String newSurname, String newName) {
        String oldStudentID = oldStudent.getID();

        if (!students.containsKey(oldStudentID)) {
            return false;
        }

        if (oldStudentID.equals(newID)) {
            oldStudent.setName(newName);
            oldStudent.setSurname(newSurname);
        } else {
            if (students.containsKey(newID)) {
                return false;
            }

            Set<Course> attendedCourses = oldStudent.getAttendedCourses();


            // Devo "clonare" i corsi frequentati perch� altrimenti rimuoverei
            // elementi da attendedCourses mentre la sto scorrendo, e il
            // programma andrebbe in
            // ConcurrentModificationException
            Set<Course> tmpAttendedCourses = new TreeSet<>();
            for (Course course : attendedCourses) {
                tmpAttendedCourses.add(course);
            }

            createStudent(newID, newSurname, newName);

            for (Course course : tmpAttendedCourses) {
                course.removeStudent(oldStudent);
                course.addStudent(students.get(newID));
            }

            removeStudent(oldStudent);
        }

        return true;
    }

    public Student getStudentByID(String studentID) {
        return students.get(studentID);
    }

    public Set<Student> getStudents() {
        return studentsBySurname;
    }
}