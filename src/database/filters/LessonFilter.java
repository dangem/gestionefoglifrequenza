package database.filters;

import java.util.Set;

import database.objects.Lesson;
import database.objects.Student;

public class LessonFilter implements Filter {

    private Lesson lesson;
    private LessonFilter otherLesson;

    public LessonFilter(Lesson lesson) {
        this.lesson = lesson;
        this.otherLesson = null;
    }

    public LessonFilter(LessonFilter otherLesson, Lesson lesson) {
        this.lesson = lesson;
        this.otherLesson = otherLesson;
    }

    @Override
    public Set<Student> search() {
        Set<Student> students = null;

        if (otherLesson == null) {
            students = lesson.getStudents();
        } else {
            students = otherLesson.search();
            students.addAll(lesson.getStudents());
        }

        return students;
    }
}
