package database.filters;

import java.util.HashSet;
import java.util.Set;

import database.Database;
import database.objects.Student;

public class IDFilter implements Filter {

	private String studentID;
	
	public IDFilter(String studentID) {
		this.studentID = studentID;
	}
	
    @Override
    public Set<Student> search() {
    	Set<Student> aStudent = new HashSet<Student>();
    	
    	// RITORNA NULL SE LO STUDENTE NON ESISTE
    	aStudent.add(Database.get().getStudentByID(studentID));
    	return aStudent;
    }
    
}
