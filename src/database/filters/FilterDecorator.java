package database.filters;

public abstract class FilterDecorator implements Filter {

    protected Filter superFilter;

    public FilterDecorator(Filter filter) {
        this.superFilter = filter;
    }
    
}
