package database.filters;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import database.Database;
import database.objects.Student;

public class CourseFilterDecorator extends FilterDecorator {
	
	private String courseToSearch;

	public CourseFilterDecorator(Filter filter, String courseToSearch) {
		super(filter);
		this.courseToSearch = courseToSearch;
	}

	@Override
	public Set<Student> search() {
		Set<Student> students = superFilter.search();
        Set<Student> filteredStudents = new TreeSet<Student>();
        Set<Student> courseStudents = Database.get()
        									.getCourseByName(courseToSearch).getStudents();
        
        // PER MOTIVI DI EFFICIENZA
        Map<String, Student> courseStudentsMap = new HashMap<String, Student>();
        for (Student student : courseStudents) {
        	courseStudentsMap.put(student.getID(), student);
        }
        
        for (Student student : students) {
        	if (courseStudentsMap.containsKey(student.getID()))
        		filteredStudents.add(student);
        }
        
        return filteredStudents;
	}

}
