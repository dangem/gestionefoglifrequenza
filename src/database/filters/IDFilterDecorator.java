package database.filters;

import java.util.Set;
import java.util.TreeSet;

import database.objects.Student;

public class IDFilterDecorator extends FilterDecorator {

    String idToSearch;

    public IDFilterDecorator(Filter filter, String idToSearch) {
        super(filter);
        this.idToSearch = idToSearch;
    }

    @Override
    public Set<Student> search() {
        Set<Student> students = superFilter.search();
        Set<Student> filteredStudents = new TreeSet<Student>();

        for (Student student : students) {
            if (student.getID().equals(idToSearch))
                filteredStudents.add(student);
        }

        return filteredStudents;
    }
}
