package database.filters;

import java.util.Set;
import database.Database;
import database.objects.Student;

public class StudentFilter implements Filter {

    @Override
    public Set<Student> search() {
    	Set<Student> students = Database.get().getStudents();
    	return students;
    }

}
