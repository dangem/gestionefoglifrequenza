package database.filters;

import java.util.Set;
import java.util.TreeSet;

import database.objects.Student;

public class NameFilterDecorator extends FilterDecorator {

    String nameToSearch;

    public NameFilterDecorator(Filter filter, String nameToSearch) {
        super(filter);
        this.nameToSearch = nameToSearch;
    }

    @Override
    public Set<Student> search() {
        Set<Student> students = superFilter.search();
        Set<Student> filteredStudents = new TreeSet<Student>();

        for (Student student : students) {
            if (student.getName().equals(nameToSearch))
                filteredStudents.add(student);
        }

        return filteredStudents;
    }
}
