package database.filters;

import java.util.Set;

import database.objects.Student;

public interface Filter {
	
    public Set<Student> search();
    
}
