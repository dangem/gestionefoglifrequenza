package database.filters;

import java.util.Set;

import database.objects.Course;
import database.objects.Student;

public class CourseFilter implements Filter {

    private Course course;
    private CourseFilter otherCourse;

    public CourseFilter(Course course) {
        this.course = course;
        this.otherCourse = null;
    }

    public CourseFilter(CourseFilter otherCourse, Course course) {
        this.course = course;
        this.otherCourse = otherCourse;
    }

    @Override
    public Set<Student> search() {
        Set<Student> students = null;

        if (otherCourse == null) {
            students = course.getStudents();
        } else {
            students = otherCourse.search();
            students.addAll(course.getStudents());
        }

        return students;
    }
}
