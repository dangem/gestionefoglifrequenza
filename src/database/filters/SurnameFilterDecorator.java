package database.filters;

import java.util.Set;
import java.util.TreeSet;

import database.objects.Student;

public class SurnameFilterDecorator extends FilterDecorator {

    private String surnameToSearch;

    public SurnameFilterDecorator(Filter filter, String surnameToSearch) {
        super(filter);
        this.surnameToSearch = surnameToSearch;
    }

    @Override
    public Set<Student> search() {
        Set<Student> students = superFilter.search();

        Set<Student> filteredStudents = new TreeSet<Student>();
        for (Student student : students) {
            if (student.getSurname().equals(surnameToSearch)) {
                filteredStudents.add(student);
            }
        }

        return filteredStudents;
    }

}
