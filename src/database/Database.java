package database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import utilities.exceptions.LessonCheckingException;
import utilities.exceptions.NewStudentAlreadyInDatabaseException;

import com.thoughtworks.xstream.XStream;

import database.filters.Filter;
import database.objects.Course;
import database.objects.CoursesManager;
import database.objects.Lesson;
import database.objects.Student;
import database.objects.StudentsManager;

public class Database {

    public static final String DATABASE_FILE_PATH = "database.xml";

    private StudentsManager studentsManager;
    private CoursesManager coursesManager;

    private static Database instance;

    private List<DatabaseListener> listeners;

    private Database() {

    }

    public static Database get() {
        if (instance == null) {
            instance = new Database();
        }

        return instance;
    }

    public void initNew() throws IOException {
        studentsManager = new StudentsManager();
        coursesManager = new CoursesManager();
        save();
    }

    public void addListener(DatabaseListener listener) {
        if (listeners == null) {
            listeners = new ArrayList<DatabaseListener>();
        }

        listeners.add(listener);
    }

    public void removeListener(DatabaseListener listener) {
        listeners.remove(listener);
    }

    /**
     * Carica il database da file.
     * 
     * @throws FileNotFoundException
     */
    public void load() throws FileNotFoundException {
        XStream xStream = getXStream();

        File database_file = new File(DATABASE_FILE_PATH);

        if (!database_file.isFile()) {
            throw new FileNotFoundException();
        }

        instance = (Database) xStream.fromXML(database_file);
    }

    /**
     * Salva il database su file.
     * 
     * @throws IOException
     */
    public void save() throws IOException {
        XStream xStream = getXStream();

        File database_file = new File(DATABASE_FILE_PATH);
        FileOutputStream fileOutputStream = new FileOutputStream(database_file);
        xStream.toXML(instance, fileOutputStream);
        fileOutputStream.close();
    }

    /**
     * Crea e restituisce un oggetto XStream per la serializzazione del database
     * su file in formato XML.
     */
    private static XStream getXStream() {
        XStream xStream = new XStream();
        xStream.alias("database", Database.class);
        xStream.omitField(Database.class, "xstream");
        xStream.omitField(Database.class, "listeners");

        return xStream;
    }

    public Set<Student> search(Filter filter) {
        return filter.search();
    }

    public boolean addStudent(String studentID, String surname, String name) {
        boolean everythingFine = studentsManager.createStudent(studentID,
                surname, name);

        notifyUpdateIfChanged(everythingFine);
        return everythingFine;
    }

    public boolean removeStudent(Student student) {
        boolean everythingFine = studentsManager.removeStudent(student);

        notifyUpdateIfChanged(everythingFine);
        return everythingFine;
    }

    public boolean editStudent(Student oldStudent, String newID,
            String newSurname, String newName) {
        boolean everythingFine = studentsManager.editStudent(oldStudent, newID,
                newSurname, newName);

        notifyUpdateIfChanged(everythingFine);
        return everythingFine;
    }

    public Student getStudentByID(String studentID) {
        return studentsManager.getStudentByID(studentID);
    }

    public boolean addCourse(String courseName) {
        boolean everythingFine = coursesManager.createCourse(courseName);

        notifyUpdateIfChanged(everythingFine);
        return everythingFine;
    }

    public boolean deleteCourse(Course course) {
        boolean everythingFine = coursesManager.deleteCourse(course);

        notifyUpdateIfChanged(everythingFine);
        return everythingFine;
    }

    public boolean renameCourse(Course course, String newCourseName) {
        boolean everythingFine = coursesManager.renameCourse(course,
                newCourseName);

        notifyUpdateIfChanged(everythingFine);
        return everythingFine;
    }

    public Course getCourseByName(String courseName) {
        return coursesManager.getCourseByName(courseName);
    }

    public Set<Course> getCourses() {
        return coursesManager.getCourses();
    }

    public Set<Student> getStudents() {
        return studentsManager.getStudents();
    }

    /**
     * Aggiunge uno studente ad un corso.
     * <p>
     * Se lo studente da aggiungere non � presente nel database, crea un nuovo
     * studente e lo aggiunge al corso.
     * 
     * @return <true> se l'aggiunta � andata a buon fine, <false> se lo studente
     *         era gi� presente nel corso.
     * @throws NewStudentAlreadyInDatabaseException
     */
    public boolean addStudentToCourse(String studentID, String surname,
            String name, Course course)
            throws NewStudentAlreadyInDatabaseException {

        if (studentsManager.getStudentByID(studentID) == null) {
            addStudent(studentID, surname, name);
        } else {
            Student alreadyInDatabase = studentsManager
                    .getStudentByID(studentID);
            if (!surname.equals(alreadyInDatabase.getSurname())
                    || (!name.equals(alreadyInDatabase.getName()))) {
                throw new NewStudentAlreadyInDatabaseException();
            }
        }

        boolean everythingFine = coursesManager.addStudentToCourse(
                studentsManager.getStudentByID(studentID), course);

        notifyUpdate();
        return everythingFine;
    }

    public boolean addListOfStudentsToCourse(List<Student> students,
            Course course) throws NewStudentAlreadyInDatabaseException {
        boolean everythingFine = true;

        for (Student student : students) {
            everythingFine = addStudentToCourse(student.getID(),
                    student.getSurname(), student.getName(), course);
        }

        notifyUpdate();
        return everythingFine;
    }

    public boolean removeStudentFromCourse(Student student, Course course) {
        boolean everythingFine = coursesManager.removeStudentFromCourse(
                student, course);

        notifyUpdateIfChanged(everythingFine);
        return everythingFine;
    }

    public boolean addStudentsToCourseFromFile(Course course, File file)
            throws IOException, NewStudentAlreadyInDatabaseException {

        boolean everythingFine = true;

        BufferedReader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(file)));

        String buffer = reader.readLine();

        while (buffer != null) {
            String[] tokens = buffer.split(" (. )?");
            everythingFine = addStudentToCourse(tokens[0], tokens[1],
                    tokens[2], course);
            buffer = reader.readLine();
        }

        reader.close();
        notifyUpdate();
        return everythingFine;
    }

    public boolean checkLesson(Course course, Lesson lesson,
            List<Boolean> presencesList) throws LessonCheckingException {
        boolean everythingFine = coursesManager.checkLesson(course, lesson,
                presencesList);

        notifyUpdateIfChanged(everythingFine);
        return everythingFine;
    }

    public Set<Lesson> getLessons(Course course) {
        return course.getLessons();
    }

    public boolean createLesson(Course course, String date) {
        boolean everythingFine = coursesManager.createLesson(course, date);

        notifyUpdateIfChanged(everythingFine);
        return everythingFine;
    }

    public boolean deleteLesson(Course course, Lesson lesson) {
        boolean everythingFine = coursesManager.deleteLesson(course, lesson);

        notifyUpdateIfChanged(everythingFine);
        return everythingFine;
    }

    public void editPresence(Course course, Lesson lesson, Student student) {
        coursesManager.editPresence(course, lesson, student);
        notifyUpdate();
    }

    private void notifyUpdate() {
        for (DatabaseListener listener : listeners) {
            listener.databaseModified();
        }
    }

    private void notifyUpdateIfChanged(boolean changed) {
        if (changed) {
            notifyUpdate();
        }
    }
}
