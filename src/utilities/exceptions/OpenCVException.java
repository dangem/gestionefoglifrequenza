package utilities.exceptions;

@SuppressWarnings("serial")
public class OpenCVException extends Exception {

    public OpenCVException(String message) {
        super(message);
    }
}
