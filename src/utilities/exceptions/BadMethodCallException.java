package utilities.exceptions;

@SuppressWarnings("serial")
public class BadMethodCallException extends Exception {

    public BadMethodCallException(String message) {
        super(message);
    }

}
