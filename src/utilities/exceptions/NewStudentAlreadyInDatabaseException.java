package utilities.exceptions;

@SuppressWarnings("serial")
public class NewStudentAlreadyInDatabaseException extends Exception {

    public NewStudentAlreadyInDatabaseException() {
        super("Uno studente con la stessa matricola ma dati diversi appartiene gi� al database. Aggiungilo al corso tramite la finestra di dialogo precedente.");
    }
}
