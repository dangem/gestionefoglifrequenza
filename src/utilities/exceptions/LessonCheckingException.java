package utilities.exceptions;

@SuppressWarnings("serial")
public class LessonCheckingException extends Exception {
	
	public LessonCheckingException() {
		super("Inconsistenza tra i dati analizzati dal foglio e quelli presenti nella lezione.");
	}

}
