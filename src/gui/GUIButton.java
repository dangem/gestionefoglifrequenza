package gui;

import gui.mediators.AbstractMediator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

@SuppressWarnings("serial")
public class GUIButton extends JButton {

    public GUIButton(String buttonName, final AbstractMediator mediator) {
        super(buttonName);

        this.setName(buttonName);

        final GUIButton thisButton = this;

        this.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                mediator.buttonClicked(thisButton);
            }
        });
    }
}
