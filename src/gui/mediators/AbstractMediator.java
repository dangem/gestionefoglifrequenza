package gui.mediators;

import gui.AbstractFrame;
import gui.GUIButton;

import javax.swing.JOptionPane;

import database.filters.Filter;
import database.filters.IDFilterDecorator;
import database.filters.NameFilterDecorator;
import database.filters.SurnameFilterDecorator;

public abstract class AbstractMediator {

    public static final int DIALOG_YES_OPTION = JOptionPane.YES_OPTION;
    public static final int DIALOG_NO_OPTION = JOptionPane.NO_OPTION;
    public static final int DIALOG_CANCEL_OPTION = JOptionPane.CANCEL_OPTION;

    protected AbstractFrame frame;

    protected Filter filter;

    public void showFrame() {
        if (frame == null) {
            initFrame();
        }

        frame.setVisible(true);
    }

    protected abstract void initFrame();

    public abstract void buttonClicked(GUIButton button);

    protected Filter getRightFilter(Filter filter, String[] studentData) {
        String studentID = studentData[0];
        String surname = studentData[1];
        String name = studentData[2];

        if (!studentID.equals("")) {
            return new IDFilterDecorator(filter, studentID);
        } else if (!surname.equals("") && name.equals("")) {
            return new SurnameFilterDecorator(filter, surname);
        } else if (surname.equals("") && !name.equals("")) {
            return new NameFilterDecorator(filter, name);
        } else if (!surname.equals("") && !name.equals("")) {
            return new SurnameFilterDecorator(new NameFilterDecorator(filter,
                    name), surname);
        }

        return null;
    }
}
