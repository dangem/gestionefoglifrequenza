package gui.mediators;

import gui.GUIButton;
import gui.GUIManager;
import gui.mainFrame.MainFrame;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JList;

import utilities.exceptions.NewStudentAlreadyInDatabaseException;
import database.Database;
import database.DatabaseListener;
import database.filters.CourseFilter;
import database.objects.Course;
import database.objects.Student;

public class MainFrameMediator extends AbstractMediator implements
        DatabaseListener {

    private GUIButton addCourseButton;
    private GUIButton deleteCourseButton;
    private GUIButton renameCourseButton;
    private GUIButton manageLessonsButton;

    private GUIButton addStudentToCourseButton;
    private GUIButton filterStudentsButton;
    private GUIButton editStudentButton;
    private GUIButton removeStudentButton;

    private JList<Student> courseStudentsList;
    private JList<Course> coursesList;

    boolean unsavedChanges;

    public MainFrameMediator() {
        unsavedChanges = false;
        Database.get().addListener(this);
    }

    @Override
    protected void initFrame() {
        frame = new MainFrame(this);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }

    @Override
    public void buttonClicked(GUIButton button) {
        if (button == addCourseButton) {
            addCourse();
        } else if (button == deleteCourseButton) {
            deleteCourse();
        } else if (button == renameCourseButton) {
            renameCourse();
        } else if (button == manageLessonsButton) {
            manageLessons();
        } else if (button == addStudentToCourseButton) {
            addStudentsToCourse();
        } else if (button == filterStudentsButton) {
            filterStudents();
        } else if (button == editStudentButton) {
            editStudent();
        } else if (button == removeStudentButton) {
            removeStudentFromCourse();
        }
    }

    public void setCoursesList(JList<Course> coursesList) {
        this.coursesList = coursesList;
        updateCoursesList();
    }

    public void setCourseStudentsList(JList<Student> studentsList) {
        this.courseStudentsList = studentsList;
        updateCourseStudentsList();
    }

    public void addCourse() {
        String courseName = GUIManager.get().showInputDialog(frame,
                "Crea Corso", "Inserisci il nome del nuovo corso:");

        if (courseName == null) {
            return;
        } else if (courseName.equals("")) {
            GUIManager.get().showErrorDialog(frame,
                    "I dati inseriti non sono validi.");
            return;
        }

        if (!Database.get().addCourse(courseName)) {
            GUIManager.get().showErrorDialog(frame,
                    "Esiste gi� un corso con questo nome.");
        }

        updateCoursesList();
    }

    public void deleteCourse() {
        Course course = coursesList.getSelectedValue();
        if (course == null) {
            return;
        }

        if (!Database.get().deleteCourse(course)) {
            GUIManager.get().showErrorDialog(frame,
                    "Errore nell'eliminazione del corso.");
        }

        updateCoursesList();
    }

    public void renameCourse() {
        Course course = coursesList.getSelectedValue();
        if (course == null) {
            return;
        }

        String newName = GUIManager.get().showInputDialog(frame,
                "Rinomina corso", "Inserisci il nuovo nome per il corso:");

        if (!Database.get().renameCourse(course, newName)) {
            GUIManager
                    .get()
                    .showErrorDialog(
                            frame,
                            "Il nome inserito non � valido, o un corso con questo nome � gi� presente nel database.");
            return;
        }

        updateCoursesList();
    }

    public void addStudentsToCourse() {
        if (coursesList.getSelectedValue() != null) {
            GUIManager.get().showAddStudentToCourseDialog(frame, this);
        }
    }

    public void addStudentsToCourseFromFile() {
        Course course = coursesList.getSelectedValue();

        if (course == null) {
            return;
        }

        boolean everythingFine = true;

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File("."));
        fileChooser
                .setDialogTitle("Seleziona la lista degli studenti da aggiungere:");
        int returnValue = fileChooser.showOpenDialog(frame);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            try {
                try {
                    everythingFine = Database.get()
                            .addStudentsToCourseFromFile(course,
                                    fileChooser.getSelectedFile());
                } catch (NewStudentAlreadyInDatabaseException e) {
                    GUIManager
                            .get()
                            .showErrorDialog(
                                    frame,
                                    "I dati di uno studente aggiunto non sono consistenti con quelli dello studente con la stessa matricola presente nel database.");
                }
            } catch (IOException e) {
                GUIManager.get().showErrorDialog(frame,
                        "Errore nella lettura del file");
            }
        }

        if (!everythingFine) {
            GUIManager.get().showInfoDialog(frame,
                    "Alcuni degli studenti appartenevano gi� al corso");
        }

        updateCourseStudentsList();
    }

    public void addStudentToCourse(String[] studentData) {
        Course course = coursesList.getSelectedValue();

        try {
            if (Database.get().addStudentToCourse(studentData[0],
                    studentData[1], studentData[2], course)) {
                updateCourseStudentsList();
            } else {
                GUIManager.get().showErrorDialog(frame,
                        "Lo studente appartiene gi� al corso.");
            }
        } catch (NewStudentAlreadyInDatabaseException e) {
            GUIManager.get().showErrorDialog(frame, e.getMessage());
        }
    }

    public void addListOfStudentsToCourse(List<Student> students) {
        Course course = coursesList.getSelectedValue();

        boolean everythingFine = true;

        try {
            everythingFine = Database.get().addListOfStudentsToCourse(students,
                    course);
        } catch (NewStudentAlreadyInDatabaseException e) {
            GUIManager.get().showErrorDialog(frame, e.getMessage());
        }

        if (!everythingFine) {
            GUIManager
                    .get()
                    .showInfoDialog(frame,
                            "Alcuni o tutti gli studenti selezionati facevano gi� parte del corso");
        }

        updateCourseStudentsList();
    }

    public void editStudent() {
        Student oldStudent = courseStudentsList.getSelectedValue();
        if (oldStudent == null) {
            return;
        }

        String[] editedStudentData = GUIManager.get().showNewStudentDialog(
                frame, oldStudent.getID(), oldStudent.getSurname(),
                oldStudent.getName());

        if (editedStudentData == null) {
            return;
        }

        if (Database.get().editStudent(oldStudent, editedStudentData[0],
                editedStudentData[1], editedStudentData[2])) {
            updateCourseStudentsList();
        } else {
            GUIManager
                    .get()
                    .showErrorDialog(frame,
                            "Il nuovo ID scelto corrisponde ad uno studente gi� esistente.");
        }
    }

    public void removeStudentFromCourse() {
        Course course = coursesList.getSelectedValue();
        Student student = courseStudentsList.getSelectedValue();
        if (course == null || student == null) {
            return;
        }

        if (Database.get().removeStudentFromCourse(student, course)) {
            updateCourseStudentsList();
        } else {
            GUIManager.get().showErrorDialog(frame,
                    "Lo studente non fa parte del corso");
        }
    }

    public void filterStudents() {
        Course course = coursesList.getSelectedValue();

        if (course == null) {
            return;
        }

        String[] studentData = GUIManager.get().showFilterDialog(frame);

        CourseFilter courseFilter = new CourseFilter(
                coursesList.getSelectedValue());

        if (studentData == null) {
            filter = null;
        } else {
            filter = getRightFilter(courseFilter, studentData);
        }

        updateCourseStudentsList();
    }

    public void manageLessons() {
        Course course = coursesList.getSelectedValue();

        if (course != null) {
            GUIManager.get().showLessonsFrame(course);
        }
    }

    public void courseSelected() {
        updateCourseStudentsList();
    }

    public void updateCoursesList() {
        Set<Course> courses = Database.get().getCourses();

        DefaultListModel<Course> listModel = new DefaultListModel<Course>();

        for (Course course : courses) {
            listModel.addElement(course);
        }

        coursesList.setModel(listModel);
        coursesList.updateUI();
    }

    public void updateCourseStudentsList() {
        Course course = coursesList.getSelectedValue();

        DefaultListModel<Student> listModel = new DefaultListModel<Student>();

        Set<Student> students;

        if (course != null) {
            if (filter != null) {
                students = filter.search();
                filterStudentsButton.setText("FILTRI ATTIVATI");
            } else {
                students = course.getStudents();
                filterStudentsButton.setText(filterStudentsButton.getName());
            }

            for (Student student : students) {
                listModel.addElement(student);
            }
        }

        courseStudentsList.setModel(listModel);
        courseStudentsList.updateUI();
    }

    public void setAddCourseButton(GUIButton button) {
        this.addCourseButton = button;
    }

    public void setDeleteCourseButton(GUIButton button) {
        this.deleteCourseButton = button;
    }

    public void setRenameCourseButton(GUIButton button) {
        this.renameCourseButton = button;
    }

    public void setManageLessonsButton(GUIButton button) {
        this.manageLessonsButton = button;
    }

    public void setAddStudentToCourseButton(GUIButton button) {
        this.addStudentToCourseButton = button;
    }

    public void setFilterStudentsButton(GUIButton button) {
        this.filterStudentsButton = button;
    }

    public void setEditStudentButton(GUIButton button) {
        this.editStudentButton = button;
    }

    public void setDeleteStudentFromCourseButton(GUIButton button) {
        this.removeStudentButton = button;
    }

    public void saveDatabase() {
        try {
            Database.get().save();
        } catch (IOException e) {
            GUIManager.get().showErrorDialog(frame, e.getMessage());
        }

        unsavedChanges = false;
        GUIManager.get().showInfoDialog(frame,
                "Salvataggio effettuato correttamente");
    }

    public void closeFrame() {
        if (unsavedChanges) {
            switch (GUIManager.get().showExitDialog(frame)) {
            case DIALOG_YES_OPTION:
                saveDatabase();
                System.exit(0);
                break;
            case DIALOG_NO_OPTION:
                System.exit(0);
            default:
                break;
            }
        } else {
            System.exit(0);
        }
    }

    @Override
    public void databaseModified() {
        unsavedChanges = true;
    }
}
