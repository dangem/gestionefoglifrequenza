package gui.mediators;

import gui.GUIButton;
import gui.GUIManager;
import gui.lessonsFrame.LessonsFrame;
import gui.panelBuilder.components.LessonStudentsListPanel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JList;

import utilities.exceptions.LessonCheckingException;
import utilities.exceptions.OpenCVException;
import app.SheetCreator;
import app.SheetScanner;
import database.Database;
import database.filters.LessonFilter;
import database.objects.Course;
import database.objects.Lesson;
import database.objects.Student;

public class LessonsFrameMediator extends AbstractMediator {

    Course course;

    private GUIButton createLessonButton;
    private GUIButton deleteLessonButton;
    private GUIButton createSheetButton;
    private GUIButton scanSheetButton;

    private GUIButton editStudentButton;
    private GUIButton showAllPresencesButton;
    private GUIButton filterStudentsButton;

    private JList<Lesson> lessonsList;
    private LessonStudentsListPanel listPanel;

    @Override
    protected void initFrame() {
        if (course != null) {
            frame = new LessonsFrame(this);
            frame.pack();
            frame.setLocationRelativeTo(null);
        } else {
            GUIManager
                    .get()
                    .showErrorDialog(
                            null,
                            "Il frame di gestione delle lezioni non � stato aperto in quanto non � stato scelto nessun corso");
        }
    }

    @Override
    public void buttonClicked(GUIButton button) {
        if (button == createLessonButton) {
            createLesson();
        } else if (button == deleteLessonButton) {
            deleteLesson();
        } else if (button == createSheetButton) {
            createSheet();
        } else if (button == scanSheetButton) {
            scanSheet();
        } else if (button == editStudentButton) {
            editStudentPresence();
        } else if (button == showAllPresencesButton) {
            showAllPresences();
        } else if (button == filterStudentsButton) {
            filterStudents();
        }
    }

    private void createLesson() {
        String date = GUIManager.get().showInputDialog(frame, "Crea lezione",
                "Inserisci la data della lezione");

        if (date == null) {
            return;
        } else if (date.equals("")) {
            GUIManager.get().showErrorDialog(frame,
                    "I dati inseriti non sono validi");
            return;
        }

        if (!Database.get().createLesson(course, date)) {
            GUIManager.get().showErrorDialog(frame,
                    "Esiste gi� una lezione in questa data");
        }

        updateLessonsList();
    }

    private void deleteLesson() {
        Lesson lesson = lessonsList.getSelectedValue();

        if (lesson == null) {
            return;
        }

        if (!Database.get().deleteLesson(course, lesson)) {
            GUIManager.get().showErrorDialog(frame,
                    "Errore nell'eliminazione della lezione.");
        }

        updateLessonsList();
        updateLessonStudentsListPanel();
    }

    private void createSheet() {
        Lesson lesson = lessonsList.getSelectedValue();

        if (lesson == null) {
            return;
        }

        SheetCreator sheetCreator = new SheetCreator(course.getName(),
                lesson.getDate(), lesson.getStudents());
        try {
            sheetCreator.createSheet();
            GUIManager.get().showInfoDialog(frame,
                    "Creazione del foglio avvenuta correttamente");
        } catch (IOException e) {
            GUIManager.get().showErrorDialog(frame, e.getMessage());
        } catch (InterruptedException e) {
            GUIManager.get().showErrorDialog(frame, e.getMessage());
        } catch (OpenCVException e) {
            GUIManager.get().showErrorDialog(frame,
                    "OpenCVException: " + e.getMessage());
        }
    }

    private void scanSheet() {
        Lesson lesson = lessonsList.getSelectedValue();

        if (lesson == null || lesson.isChecked()) {
            return;
        }

        JFileChooser templateFileChooser = new JFileChooser();
        templateFileChooser.setCurrentDirectory(new File("."));
        templateFileChooser
                .setDialogTitle("Scegli il foglio di frequenza originale:");
        int templateReturnValue = templateFileChooser.showOpenDialog(frame);

        if (templateReturnValue != JFileChooser.APPROVE_OPTION) {
            return;
        }

        String templateFilePath = templateFileChooser.getSelectedFile()
                .getPath();

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File("."));
        fileChooser.setDialogTitle("Scegli l'immagine da analizzare:");
        int returnValue = fileChooser.showOpenDialog(frame);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            String sheetFilePath = fileChooser.getSelectedFile().getPath();
            String presencesFilePath = course.getName() + "_"
                    + lesson.getDate() + "_" + "presences";

            SheetScanner sheetScanner = new SheetScanner(templateFilePath,
                    sheetFilePath, presencesFilePath);

            List<Boolean> presencesList;

            try {
                presencesList = sheetScanner.scanSheet();
                checkLesson(lesson, presencesList);
            } catch (IOException e) {
                GUIManager.get().showErrorDialog(frame, e.getMessage());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (OpenCVException e) {
                GUIManager.get().showErrorDialog(frame,
                        "OpenCVException: " + e.getMessage());
            } catch (LessonCheckingException e) {
                GUIManager
                        .get()
                        .showErrorDialog(frame,
                                "Errore di coerenza tra i dati memorizzati e quelli acquisiti dal foglio");
            }
        }
    }

    private void checkLesson(Lesson lesson, List<Boolean> presencesList)
            throws LessonCheckingException {
        if (Database.get().checkLesson(course, lesson, presencesList)) {
            GUIManager.get().showInfoDialog(frame,
                    "Acquisizione dei dati avvenuta correttamente");
        } else {
            GUIManager.get().showErrorDialog(frame,
                    "Errore nell'acquisizione dei dati");
        }

        updateLessonStudentsListPanel();
    }

    private void editStudentPresence() {
        Lesson lesson = lessonsList.getSelectedValue();
        Student student = listPanel.getSelectedStudent();

        if (lesson == null || student == null) {
            return;
        }

        Database.get().editPresence(course, lesson, student);

        updateLessonStudentsListPanel();
    }

    public void filterStudents() {
        Lesson lesson = lessonsList.getSelectedValue();

        if (lesson == null) {
            return;
        }

        String[] studentData = GUIManager.get().showFilterDialog(frame);

        LessonFilter lessonFilter = new LessonFilter(
                lessonsList.getSelectedValue());

        if (studentData == null) {
            filter = null;
        } else {
            filter = getRightFilter(lessonFilter, studentData);
        }

        updateLessonStudentsListPanel();
    }

    private void showAllPresences() {
        GUIManager.get().showAllPresencesDialog(frame, course);
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Course getCourse() {
        return course;
    }

    public void setCreateLessonButton(GUIButton createLessonButton) {
        this.createLessonButton = createLessonButton;
    }

    public void setDeleteLessonButton(GUIButton deleteLessonButton) {
        this.deleteLessonButton = deleteLessonButton;
    }

    public void setCreateSheetButton(GUIButton createSheetButton) {
        this.createSheetButton = createSheetButton;
    }

    public void setScanSheetButton(GUIButton scanSheetButton) {
        this.scanSheetButton = scanSheetButton;
    }

    public void setEditStudentButton(GUIButton editStudentButton) {
        this.editStudentButton = editStudentButton;
    }

    public void setShowAllPresencesButton(GUIButton showAllPresencesButton) {
        this.showAllPresencesButton = showAllPresencesButton;
    }

    public void setFilterStudentsButton(GUIButton filterStudentsButton) {
        this.filterStudentsButton = filterStudentsButton;
    }

    public void setLessonsList(JList<Lesson> lessonsList) {
        this.lessonsList = lessonsList;
        updateLessonsList();
    }

    public void setLessonStudentsListPanel(LessonStudentsListPanel listPanel) {
        this.listPanel = listPanel;
        updateLessonStudentsListPanel();
    }

    public void lessonSelected() {
        updateLessonStudentsListPanel();
    }

    public void updateLessonsList() {
        Set<Lesson> lessons = Database.get().getLessons(course);

        DefaultListModel<Lesson> listModel = new DefaultListModel<Lesson>();

        for (Lesson lesson : lessons) {
            listModel.addElement(lesson);
        }

        lessonsList.setModel(listModel);
        lessonsList.updateUI();
    }

    public void updateLessonStudentsListPanel() {
        Lesson lesson = lessonsList.getSelectedValue();

        if (lesson == null) {
            listPanel.empty();
            return;
        }

        Set<Student> students;

        if (filter == null) {
            students = lesson.getStudents();
            filterStudentsButton.setText(filterStudentsButton.getName());
        } else {
            students = filter.search();
            filterStudentsButton.setText("FILTRI ATTIVATI");
        }

        List<String> presences = new ArrayList<String>();

        for (Student student : students) {
            if (lesson.isPresent(student)) {
                presences.add("         PRESENTE");
            } else {
                presences.add("         ASSENTE");
            }
        }

        listPanel.update(students, presences);
    }
}
