package gui.panelBuilder.components;

import javax.swing.JPanel;

/**
 * Rappresenta un'astrazione per il "classico" pannello dell'applicazione
 * costruito tramite PanelBuilder, e composto da un pannello dei bottoni in alto
 * e una lista in basso.
 */
@SuppressWarnings("serial")
public class BuiltPanel extends JPanel {

    public static final int TOP_BORDER_SIZE = 20;
    public static final int LEFT_BORDER_SIZE = 5;
    public static final int BOTTOM_BORDER_SIZE = 0;
    public static final int RIGHT_BORDER_SIZE = 5;

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
