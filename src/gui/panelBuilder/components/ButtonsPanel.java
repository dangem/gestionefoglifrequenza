package gui.panelBuilder.components;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ButtonsPanel extends JPanel {

    public static final int PANEL_ROWS = 2;
    public static final int PANEL_COLUMNS = 2;

    public static final int HORIZONTAL_GAP = 5;
    public static final int VERTICAL_GAP = 10;

    public static final int TOP_BORDER_SIZE = 5;
    public static final int LEFT_BORDER_SIZE = 5;
    public static final int BOTTOM_BORDER_SIZE = 10;
    public static final int RIGHT_BORDER_SIZE = 5;
}
