package gui.panelBuilder.components;

import java.awt.BorderLayout;
import java.util.List;
import java.util.Set;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;

import database.objects.Student;

@SuppressWarnings("serial")
public class LessonStudentsListPanel extends JPanel {

    private JList<Student> lessonStudentsList;
    private JList<String> presencesList;

    public LessonStudentsListPanel(JList<Student> lessonStudentsList,
            JList<String> presencesList) {
        super();
        this.lessonStudentsList = lessonStudentsList;
        this.presencesList = presencesList;
        this.presencesList.setEnabled(false);
        
        this.setLayout(new BorderLayout());
        
        this.add(lessonStudentsList, BorderLayout.WEST);
        this.add(presencesList, BorderLayout.CENTER);
    }

    public void update(Set<Student> students, List<String> presences) {
        DefaultListModel<Student> studentsListModel = new DefaultListModel<Student>();
        DefaultListModel<String> presencesListModel = new DefaultListModel<String>();

        for (Student student : students) {
            studentsListModel.addElement(student);
        }

        for (String str : presences) {
            presencesListModel.addElement(str);
        }

        lessonStudentsList.setModel(studentsListModel);
        presencesList.setModel(presencesListModel);

        lessonStudentsList.updateUI();
        presencesList.updateUI();
    }

    public Student getSelectedStudent() {
        return lessonStudentsList.getSelectedValue();
    }

    public void empty() {
        DefaultListModel<Student> studentsListModel = new DefaultListModel<Student>();
        DefaultListModel<String> presencesListModel = new DefaultListModel<String>();
        
        lessonStudentsList.setModel(studentsListModel);
        presencesList.setModel(presencesListModel);

        lessonStudentsList.updateUI();
        presencesList.updateUI();
    }
}
