package gui.panelBuilder.components;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ListPanel extends JPanel {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
