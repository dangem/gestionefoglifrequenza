package gui.panelBuilder;

import gui.panelBuilder.builders.AbstractBuilder;

public class Director {

    AbstractBuilder builder;

    public Director(AbstractBuilder builder) {
        setBuilder(builder);
    }

    public void setBuilder(AbstractBuilder builder) {
        this.builder = builder;
    }

    public void buildPanel() {
        builder.buildOuterPanel();
        builder.setPanelTitle();
        builder.buildInnerPanel();
        builder.createButtonsPanel();
        builder.addBorderToButtonsPanel();
        builder.buildButtonsPanel();
        builder.createListPanel();
        builder.setListPanelTitle();
        builder.addBorderToListPanel();
        builder.buildListPanel();
        builder.compose();
    }
}
