package gui.panelBuilder.builders;

import gui.GUIButton;
import gui.mediators.LessonsFrameMediator;
import gui.panelBuilder.components.LessonStudentsListPanel;

import java.awt.BorderLayout;

import javax.swing.JList;
import javax.swing.JScrollPane;

import database.objects.Student;

public class LessonStudentsPanelBuilder extends AbstractBuilder {

    LessonsFrameMediator mediator;

    public LessonStudentsPanelBuilder(LessonsFrameMediator mediator) {
        this.mediator = mediator;
    }

    @Override
    public void setPanelTitle() {
        panel.setTitle("Presenze per la lezione selezionata");
    }

    @Override
    public void buildButtonsPanel() {
        GUIButton editStudentButton = new GUIButton(
                "Modifica presenza studente", mediator);

        GUIButton showAllPresencesButton = new GUIButton(
                "Mostra totale presenze del corso", mediator);
        
        GUIButton filterStudentsButton = new GUIButton("Filtra studenti", mediator);

        buttonsPanel.add(editStudentButton);
        mediator.setEditStudentButton(editStudentButton);

        buttonsPanel.add(showAllPresencesButton);
        mediator.setShowAllPresencesButton(showAllPresencesButton);
        
        buttonsPanel.add(filterStudentsButton);
        mediator.setFilterStudentsButton(filterStudentsButton);
    }

    @Override
    public void setListPanelTitle() {
        listPanel.setTitle("Studenti");
    }

    @Override
    public void buildListPanel() {
        JList<Student> lessonStudentsList = new JList<Student>();
        JList<String> presences = new JList<String>();

        LessonStudentsListPanel innerListPanel = new LessonStudentsListPanel(
                lessonStudentsList, presences);

        mediator.setLessonStudentsListPanel(innerListPanel);
        JScrollPane listScrollPane = new JScrollPane(innerListPanel);
        listPanel.add(listScrollPane, BorderLayout.CENTER);
    }
}
