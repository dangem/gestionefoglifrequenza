package gui.panelBuilder.builders;

import gui.GUIButton;
import gui.mediators.LessonsFrameMediator;

import java.awt.BorderLayout;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import database.objects.Lesson;

public class ManageLessonsPanelBuilder extends AbstractBuilder {

    LessonsFrameMediator mediator;

    public ManageLessonsPanelBuilder(LessonsFrameMediator mediator) {
        this.mediator = mediator;
    }

    @Override
    public void setPanelTitle() {
        panel.setTitle("Elenco lezioni di " + mediator.getCourse().getName());
    }

    @Override
    public void buildButtonsPanel() {
        GUIButton createLessonButton = new GUIButton("Crea lezione", mediator);
        GUIButton deleteLessonButton = new GUIButton("Rimuovi lezione",
                mediator);
        GUIButton createSheetButton = new GUIButton("Crea foglio di frequenza",
                mediator);
        GUIButton scanSheetButton = new GUIButton(
                "Analizza foglio di frequenza", mediator);

        buttonsPanel.add(createLessonButton);
        mediator.setCreateLessonButton(createLessonButton);

        buttonsPanel.add(deleteLessonButton);
        mediator.setDeleteLessonButton(deleteLessonButton);

        buttonsPanel.add(createSheetButton);
        mediator.setCreateSheetButton(createSheetButton);

        buttonsPanel.add(scanSheetButton);
        mediator.setScanSheetButton(scanSheetButton);
    }

    @Override
    public void setListPanelTitle() {
        listPanel.setTitle("Lezioni");
    }

    @Override
    public void buildListPanel() {
        JList<Lesson> lessonsList = new JList<Lesson>();
        mediator.setLessonsList(lessonsList);
        JScrollPane listScrollPane = new JScrollPane(lessonsList);
        listPanel.add(listScrollPane, BorderLayout.CENTER);
        lessonsList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                mediator.lessonSelected();
            }
        });
    }
}