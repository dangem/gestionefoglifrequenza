package gui.panelBuilder.builders;

import gui.GUIButton;
import gui.mediators.MainFrameMediator;

import java.awt.BorderLayout;

import javax.swing.JList;
import javax.swing.JScrollPane;

import database.objects.Student;

public class CourseStudentsPanelBuilder extends AbstractBuilder {

    private MainFrameMediator mediator;

    public CourseStudentsPanelBuilder(MainFrameMediator mediator) {
        this.mediator = mediator;
    }

    @Override
    public void setPanelTitle() {
        panel.setTitle("Studenti");
    }

    @Override
    public void buildButtonsPanel() {
        GUIButton addStudentToCourseButton = new GUIButton(
                "Aggiungi studenti al corso", mediator);
        GUIButton deleteStudentButton = new GUIButton(
                "Rimuovi studente dal corso", mediator);
        GUIButton editStudentButton = new GUIButton("Modifica studente",
                mediator);
        GUIButton filterStudentsButton = new GUIButton("Filtra studenti",
                mediator);

        buttonsPanel.add(addStudentToCourseButton);
        mediator.setAddStudentToCourseButton(addStudentToCourseButton);

        buttonsPanel.add(editStudentButton);
        mediator.setEditStudentButton(editStudentButton);

        buttonsPanel.add(deleteStudentButton);
        mediator.setDeleteStudentFromCourseButton(deleteStudentButton);

        buttonsPanel.add(filterStudentsButton);
        mediator.setFilterStudentsButton(filterStudentsButton);
    }

    @Override
    public void setListPanelTitle() {
        listPanel.setTitle("Elenco studenti del corso selezionato");
    }

    @Override
    public void buildListPanel() {
        JList<Student> courseStudentsList = new JList<Student>();
        mediator.setCourseStudentsList(courseStudentsList);
        JScrollPane listScrollPane = new JScrollPane(courseStudentsList);
        listPanel.add(listScrollPane, BorderLayout.CENTER);
    }
}
