package gui.panelBuilder.builders;

import gui.panelBuilder.components.BuiltPanel;
import gui.panelBuilder.components.ButtonsPanel;
import gui.panelBuilder.components.ListPanel;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public abstract class AbstractBuilder {

    BuiltPanel panel;

    JPanel innerPanel;
    ButtonsPanel buttonsPanel;
    ListPanel listPanel;

    public void buildOuterPanel() {
        panel = new BuiltPanel();

        panel.setLayout(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(
                BuiltPanel.TOP_BORDER_SIZE, BuiltPanel.LEFT_BORDER_SIZE,
                BuiltPanel.BOTTOM_BORDER_SIZE, BuiltPanel.RIGHT_BORDER_SIZE));
    }

    public abstract void setPanelTitle();

    public void buildInnerPanel() {
        innerPanel = new JPanel();
        innerPanel.setLayout(new BorderLayout());

        Font titleFont = new Font("panelFont", Font.BOLD, 16);
        Border etchedBorder = BorderFactory
                .createEtchedBorder(EtchedBorder.LOWERED);

        innerPanel.setBorder(BorderFactory.createTitledBorder(etchedBorder,
                panel.getTitle(), TitledBorder.LEFT, TitledBorder.TOP,
                titleFont));
    }

    public void createButtonsPanel() {
        buttonsPanel = new ButtonsPanel();
        buttonsPanel.setLayout(new GridLayout(ButtonsPanel.PANEL_ROWS,
                ButtonsPanel.PANEL_COLUMNS, ButtonsPanel.HORIZONTAL_GAP,
                ButtonsPanel.VERTICAL_GAP));
    }

    public void addBorderToButtonsPanel() {
        buttonsPanel.setBorder(BorderFactory
                .createEmptyBorder(ButtonsPanel.TOP_BORDER_SIZE,
                        ButtonsPanel.LEFT_BORDER_SIZE,
                        ButtonsPanel.BOTTOM_BORDER_SIZE,
                        ButtonsPanel.RIGHT_BORDER_SIZE));
    }

    public abstract void buildButtonsPanel();

    public void createListPanel() {
        listPanel = new ListPanel();
        listPanel.setLayout(new BorderLayout());
    }

    public abstract void setListPanelTitle();

    public void addBorderToListPanel() {
        Font titleFont = new Font("coursesFont", Font.BOLD, 14);
        Border etchedBorder = BorderFactory
                .createEtchedBorder(EtchedBorder.LOWERED);
        listPanel.setBorder(BorderFactory.createTitledBorder(etchedBorder,
                listPanel.getTitle(), TitledBorder.LEFT, TitledBorder.TOP,
                titleFont));
    }

    public abstract void buildListPanel();

    public void compose() {
        innerPanel.add(buttonsPanel, BorderLayout.NORTH);
        innerPanel.add(listPanel, BorderLayout.CENTER);
        panel.add(innerPanel, BorderLayout.CENTER);
    }

    public BuiltPanel getPanel() {
        return panel;
    }
}
