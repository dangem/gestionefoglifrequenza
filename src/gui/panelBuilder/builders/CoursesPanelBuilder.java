package gui.panelBuilder.builders;

import gui.GUIButton;
import gui.mediators.MainFrameMediator;

import java.awt.BorderLayout;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import database.objects.Course;

public class CoursesPanelBuilder extends AbstractBuilder {

    private MainFrameMediator mediator;

    public CoursesPanelBuilder(MainFrameMediator mediator) {
        this.mediator = mediator;
    }

    @Override
    public void setPanelTitle() {
        panel.setTitle("Corsi");
    }

    @Override
    public void buildButtonsPanel() {
        GUIButton addCourseButton = new GUIButton("Aggiungi corso", mediator);
        GUIButton deleteCourseButton = new GUIButton("Elimina corso", mediator);
        GUIButton renameCourseButton = new GUIButton("Rinomina corso", mediator);
        GUIButton manageLessons = new GUIButton("Gestisci lezioni del corso",
                mediator);

        buttonsPanel.add(addCourseButton);
        mediator.setAddCourseButton(addCourseButton);

        buttonsPanel.add(deleteCourseButton);
        mediator.setDeleteCourseButton(deleteCourseButton);

        buttonsPanel.add(renameCourseButton);
        mediator.setRenameCourseButton(renameCourseButton);

        buttonsPanel.add(manageLessons);
        mediator.setManageLessonsButton(manageLessons);
    }

    public void setListPanelTitle() {
        listPanel.setTitle("Elenco Corsi");
    }

    @Override
    public void buildListPanel() {
        JList<Course> coursesList = new JList<Course>();
        mediator.setCoursesList(coursesList);
        JScrollPane listScrollPane = new JScrollPane(coursesList);
        listPanel.add(listScrollPane, BorderLayout.CENTER);
        coursesList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                mediator.courseSelected();
            }
        });
    }
}
