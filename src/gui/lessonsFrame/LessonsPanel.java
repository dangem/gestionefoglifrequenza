package gui.lessonsFrame;

import gui.mediators.LessonsFrameMediator;
import gui.panelBuilder.Director;
import gui.panelBuilder.builders.LessonStudentsPanelBuilder;
import gui.panelBuilder.builders.ManageLessonsPanelBuilder;
import gui.panelBuilder.components.BuiltPanel;

import java.awt.GridLayout;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class LessonsPanel extends JPanel {

    private static final int ROWS = 1;
    private static final int COLUMNS = 2;

    public LessonsPanel(LessonsFrameMediator mediator) {
        this.setLayout(new GridLayout(ROWS, COLUMNS));

        ManageLessonsPanelBuilder mlpbuilder = new ManageLessonsPanelBuilder(
                mediator);
        Director director = new Director(mlpbuilder);
        director.buildPanel();
        BuiltPanel manageLessonsPanel = mlpbuilder.getPanel();

        LessonStudentsPanelBuilder lspbuilder = new LessonStudentsPanelBuilder(
                mediator);
        director.setBuilder(lspbuilder);
        director.buildPanel();
        BuiltPanel LessonStudentsPanel = lspbuilder.getPanel();

        this.add(manageLessonsPanel);
        this.add(LessonStudentsPanel);
    }
}