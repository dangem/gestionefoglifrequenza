package gui.lessonsFrame;

import gui.AbstractFrame;
import gui.mediators.LessonsFrameMediator;

import java.awt.BorderLayout;

@SuppressWarnings("serial")
public class LessonsFrame extends AbstractFrame {

    public LessonsFrame(String frameName) {
        super(frameName);
    }

    private final static String FRAME_NAME = "Gestione Fogli di Frequenza";

    private LessonsPanel lessonsPanel;

    public LessonsFrame(final LessonsFrameMediator mediator) {
        super(FRAME_NAME);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        this.setLayout(new BorderLayout());

        this.lessonsPanel = new LessonsPanel(mediator);
        this.add(lessonsPanel, BorderLayout.CENTER);
    }
}
