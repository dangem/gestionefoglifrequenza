package gui;

import gui.customDialogs.addStudentToCourse.AddStudentToCourseDialog;
import gui.customDialogs.allPresencesDialog.AllPresencesDialog;
import gui.customDialogs.studentDataDialog.FilterDialog;
import gui.customDialogs.studentDataDialog.NewStudentDialog;
import gui.mediators.LessonsFrameMediator;
import gui.mediators.MainFrameMediator;

import java.awt.Component;

import javax.swing.JOptionPane;

import database.objects.Course;

public final class GUIManager {

    private static GUIManager instance;

    private MainFrameMediator mainFrameMediator;
    private LessonsFrameMediator lessonsFrameMediator;

    private GUIManager() {
        mainFrameMediator = new MainFrameMediator();
        lessonsFrameMediator = new LessonsFrameMediator();
    }

    public static GUIManager get() {
        if (instance == null) {
            instance = new GUIManager();
        }

        return instance;
    }

    public void showMainFrame() {
        mainFrameMediator.showFrame();
    }

    public void showLessonsFrame(Course course) {
        lessonsFrameMediator.setCourse(course);
        lessonsFrameMediator.showFrame();
    }

    public void databaseNotFoundDialog() {
        JOptionPane
                .showMessageDialog(
                        null,
                        "File del database non trovato. Verr� creato un nuovo database.",
                        "Database non trovato", JOptionPane.INFORMATION_MESSAGE);
    }

    public void showIOErrorDialog() {
        JOptionPane.showMessageDialog(null,
                "Errore nel salvataggio del database. Il programma terminer�.",
                "Errore I/O", JOptionPane.ERROR_MESSAGE);
    }

    public void showInfoDialog(Component parentComponent, String message) {
        JOptionPane.showMessageDialog(parentComponent, message, "Informazione",
                JOptionPane.INFORMATION_MESSAGE);
    }

    public void showErrorDialog(Component parentComponent, String message) {
        JOptionPane.showMessageDialog(parentComponent, message, "Errore",
                JOptionPane.ERROR_MESSAGE);
    }

    public String showInputDialog(Component parentComponent, String title,
            String message) {
        return JOptionPane.showInputDialog(parentComponent, message, title,
                JOptionPane.QUESTION_MESSAGE);

    }

    public int showExitDialog(Component parentComponent) {
        String exitAndSave = "Esci e salva le modifiche";
        String justExit = "Esci senza salvare le modifiche";
        String cancel = "Annulla";

        Object[] options = { exitAndSave, justExit, cancel };

        return JOptionPane.showOptionDialog(parentComponent,
                "Vuoi uscire e salvare le modifiche al database?", "Esci",
                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                null, options, options[0]);
    }

    public String[] showNewStudentDialog(Component parent) {
        return showNewStudentDialog(parent, "", "", "");
    }

    public String[] showNewStudentDialog(Component parent, String studentID,
            String surname, String name) {
        NewStudentDialog dialog = new NewStudentDialog(parent, studentID,
                surname, name);
        return dialog.showDialog();
    }

    public String[] showFilterDialog(Component parent) {
        FilterDialog dialog = new FilterDialog(parent);
        return dialog.showDialog();
    }

    public void showAllPresencesDialog(Component parent, Course course) {
        AllPresencesDialog dialog = new AllPresencesDialog(parent, course);
        dialog.showDialog();
    }

    public void showAddStudentToCourseDialog(AbstractFrame parent,
            MainFrameMediator mediator) {
        AddStudentToCourseDialog dialog = new AddStudentToCourseDialog(parent,
                mediator);
        dialog.showDialog();
    }
}
