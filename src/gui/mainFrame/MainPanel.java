package gui.mainFrame;

import gui.mediators.MainFrameMediator;
import gui.panelBuilder.Director;
import gui.panelBuilder.builders.CourseStudentsPanelBuilder;
import gui.panelBuilder.builders.CoursesPanelBuilder;
import gui.panelBuilder.components.BuiltPanel;

import java.awt.GridLayout;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MainPanel extends JPanel {

    private static final int ROWS = 1;
    private static final int COLUMNS = 2;

    public MainPanel(MainFrameMediator mediator) {
        this.setLayout(new GridLayout(ROWS, COLUMNS));

        CoursesPanelBuilder cpbuilder = new CoursesPanelBuilder(mediator);
        Director director = new Director(cpbuilder);
        director.buildPanel();
        BuiltPanel coursesPanel = cpbuilder.getPanel();

        CourseStudentsPanelBuilder cspbuilder = new CourseStudentsPanelBuilder(
                mediator);
        director.setBuilder(cspbuilder);
        director.buildPanel();
        BuiltPanel studentsPanel = cspbuilder.getPanel();

        this.add(coursesPanel);
        this.add(studentsPanel);
    }
}
