package gui.mainFrame;

import gui.AbstractFrame;
import gui.mediators.MainFrameMediator;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

@SuppressWarnings("serial")
public class MainFrame extends AbstractFrame {

    private final static String FRAME_NAME = "Gestione Fogli di Frequenza";

    private MainPanel mainPanel;
    private JMenuBar menuBar;

    public MainFrame(final MainFrameMediator mediator) {
        super(FRAME_NAME);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        this.setLayout(new BorderLayout());

        this.mainPanel = new MainPanel(mediator);
        this.add(mainPanel, BorderLayout.CENTER);

        menuBar = new JMenuBar();
        this.add(menuBar, BorderLayout.NORTH);

        JMenu menu = new JMenu("File");
        menuBar.add(menu);

        JMenuItem saveMenuItem = new JMenuItem("Salva");
        menu.add(saveMenuItem);

        saveMenuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                mediator.saveDatabase();
            }
        });

        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                mediator.closeFrame();
            }
        });
    }

    public void save() {

    }
}
