package gui;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public abstract class AbstractFrame extends JFrame {

    public AbstractFrame(String frameName) {
        super(frameName);
    }

}
