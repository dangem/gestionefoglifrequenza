package gui.customDialogs.studentDataDialog;

import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public abstract class StudentDataDialog extends Component {

    protected static final String[] STRINGS = { "Matricola", "Cognome", "Nome" };

    protected JDialog dialog;
    protected JOptionPane optionPane;

    protected JTextField idTextField;
    protected JTextField surnameTextField;
    protected JTextField nameTextField;

    protected String dialogTitle;
    protected String[] options;

    public StudentDataDialog(Component parent, String dialogTitle,
            String[] options, String initialID, String initialSurname,
            String initialName) {
        this.idTextField = new JTextField(initialID);
        this.surnameTextField = new JTextField(initialSurname);
        this.nameTextField = new JTextField(initialName);

        this.dialogTitle = dialogTitle;
        this.options = options;

        Object[] message = { STRINGS[0], idTextField, STRINGS[1],
                surnameTextField, STRINGS[2], nameTextField };

        this.optionPane = new JOptionPane(message,
                JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION,
                null, options);

        this.dialog = optionPane.createDialog(parent, dialogTitle);
        this.dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);

        this.dialog.addWindowListener(new WindowAdapter() {
            
            @Override
            public void windowOpened(WindowEvent e) {
                idTextField.requestFocusInWindow();
            }
        });
    }

    public abstract String[] showDialog();
}