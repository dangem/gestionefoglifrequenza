package gui.customDialogs.studentDataDialog;

import java.awt.Component;

import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

@SuppressWarnings("serial")
public class FilterDialog extends StudentDataDialog {

    private final static String DIALOG_TITLE = "Filtra studenti";
    private final static String[] OPTIONS = { "Applica filtro",
            "Rimuovi tutti i filtri" };

    public FilterDialog(Component parent) {
        super(parent, DIALOG_TITLE, OPTIONS, "", "", "");
        idTextField.addCaretListener(buildIDTextFieldListener());
        surnameTextField.addCaretListener(buildSurnameAndNameListener());
        nameTextField.addCaretListener(buildSurnameAndNameListener());
    }

    @Override
    public String[] showDialog() {
        dialog.setVisible(true);

        String selectedValue = (String) optionPane.getValue();

        if (selectedValue.equals(OPTIONS[0])) {
            String[] returnValues = { idTextField.getText(),
                    surnameTextField.getText(), nameTextField.getText() };

            return returnValues;
        }

        return null;
    }

    public CaretListener buildIDTextFieldListener() {
        return new CaretListener() {

            @Override
            public void caretUpdate(CaretEvent e) {
                if (!idTextField.getText().equals("")) {
                    surnameTextField.setEnabled(false);
                    surnameTextField.setText("");
                    nameTextField.setEnabled(false);
                    nameTextField.setText("");
                } else {
                    surnameTextField.setEnabled(true);
                    nameTextField.setEnabled(true);
                }
            }
        };
    }

    public CaretListener buildSurnameAndNameListener() {
        return new CaretListener() {

            @Override
            public void caretUpdate(CaretEvent e) {
                if (!surnameTextField.getText().equals("")
                        || !nameTextField.getText().equals("")) {
                    idTextField.setEnabled(false);
                    idTextField.setText("");
                } else {
                    idTextField.setEnabled(true);
                }
            }
        };
    }
}
