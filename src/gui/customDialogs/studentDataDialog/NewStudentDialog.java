package gui.customDialogs.studentDataDialog;

import gui.GUIManager;

import java.awt.Component;

@SuppressWarnings("serial")
public class NewStudentDialog extends StudentDataDialog {

    private final static String DIALOG_TITLE = "Aggiungi studente";
    private final static String[] OPTIONS = { "OK", "Annulla" };

    public NewStudentDialog(Component parent, String initialID,
            String initialSurname, String initialName) {
        super(parent, DIALOG_TITLE, OPTIONS, initialID, initialSurname,
                initialName);
    }

    /**
     * Genera una finestra di dialogo in cui si richiedono le informazioni dello
     * studente da inserire. Restituisce <b>null</b> se l'inserimento non �
     * andato a buon fine.
     * 
     * @return Un array di String cos� costituito: <br>
     *         array[0]: matricola dello studente <br>
     *         array[1]: cognome dello studente <br>
     *         array[2]: nome dello studente.
     */
    public String[] showDialog() {
        dialog.setVisible(true);

        String selectedValue = (String) optionPane.getValue();

        if (selectedValue == OPTIONS[0]) {
            if (inputIsValid()) {
                String[] returnValues = { idTextField.getText(),
                        surnameTextField.getText(), nameTextField.getText() };

                return returnValues;
            } else {
                GUIManager.get().showErrorDialog(dialog,
                        "I dati inseriti non sono validi.");
            }
        }

        return null;
    }

    /**
     * Restituisce <b>true</b> se l'input inserito dall'utente � valido.
     */
    private boolean inputIsValid() {
        return !(idTextField.getText().equals("")
                || surnameTextField.getText().equals("") || nameTextField
                .getText().equals(""));
    }
}