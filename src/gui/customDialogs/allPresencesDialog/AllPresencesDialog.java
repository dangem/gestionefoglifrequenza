package gui.customDialogs.allPresencesDialog;

import java.awt.Component;

import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import database.objects.Course;
import database.objects.Student;

@SuppressWarnings("serial")
public class AllPresencesDialog extends Component {

    private static final String DIALOG_TITLE = "Totale delle presenze";

    Course course;

    JDialog dialog;
    JOptionPane optionPane;

    JList<String> allPresences;

    public AllPresencesDialog(Component parent, Course course) {
        this.course = course;
        allPresences = new JList<String>();
        JScrollPane scrollPane = new JScrollPane(allPresences);

        this.optionPane = new JOptionPane(scrollPane,
                JOptionPane.QUESTION_MESSAGE, JOptionPane.DEFAULT_OPTION);

        populateList();

        this.dialog = optionPane.createDialog(parent, DIALOG_TITLE);
        this.dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    }

    private void populateList() {
        DefaultListModel<String> listModel = new DefaultListModel<String>();

        for (Student student : course.getStudents()) {
            int presences = course.getPresences(student);
            listModel.addElement(student.toString() + " - " + presences + "       ");
        }

        allPresences.setModel(listModel);
    }

    public void showDialog() {
        dialog.setVisible(true);
    }
}
