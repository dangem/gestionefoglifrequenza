package gui.customDialogs.addStudentToCourse;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

@SuppressWarnings("serial")
public class AddStudentsFromFileButton extends JButton {

    private final static String BUTTON_TEXT = "Aggiungi studenti da file";

    public AddStudentsFromFileButton(final AddStudentToCourseDialog parent) {
        super(BUTTON_TEXT);

        this.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                parent.addStudentsFromFile();
            }
        });
    }
}
