package gui.customDialogs.addStudentToCourse;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

@SuppressWarnings("serial")
public class NewStudentButton extends JButton {

    private final static String BUTTON_TEXT = "Aggiungi un nuovo studente";

    public NewStudentButton(final AddStudentToCourseDialog parent) {
        super(BUTTON_TEXT);

        this.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                parent.addNewStudent();
            }
        });
    }
}
