package gui.customDialogs.addStudentToCourse;

import gui.AbstractFrame;
import gui.GUIManager;
import gui.mediators.MainFrameMediator;

import java.awt.Component;
import java.awt.Dimension;
import java.util.Set;

import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import database.Database;
import database.objects.Student;

@SuppressWarnings("serial")
public class AddStudentToCourseDialog extends Component {

    private static final String DIALOG_TITLE = "Aggiungi studenti ad un corso";

    JDialog dialog;
    JOptionPane optionPane;

    JList<Student> studentsList;

    AddSelectedStudentsButton addSelectedStudentsButton;
    AddStudentsFromFileButton addStudentsFromFileButton;
    NewStudentButton newStudentButton;

    MainFrameMediator mediator;

    public AddStudentToCourseDialog(AbstractFrame parent,
            MainFrameMediator mediator) {
        this.mediator = mediator;

        this.studentsList = getStudentsList();
        JScrollPane listScrollPane = new JScrollPane(studentsList);

        this.addSelectedStudentsButton = new AddSelectedStudentsButton(this);
        this.addStudentsFromFileButton = new AddStudentsFromFileButton(this);
        this.newStudentButton = new NewStudentButton(this);

        Object[] objectsToDisplay = { listScrollPane,
                Box.createRigidArea(new Dimension(10, 10)),
                addSelectedStudentsButton, addStudentsFromFileButton,
                newStudentButton };

        String[] options = { "Annulla" };

        optionPane = new JOptionPane(objectsToDisplay,
                JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION, null,
                options);

        this.dialog = optionPane.createDialog(parent, DIALOG_TITLE);
        this.dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    }

    private JList<Student> getStudentsList() {
        DefaultListModel<Student> listModel = new DefaultListModel<Student>();

        JList<Student> studentsList = new JList<Student>(listModel);
        ListSelectionModel selectionModel = new DefaultListSelectionModel() {

            @Override
            public void setSelectionInterval(int index0, int index1) {
                if (super.isSelectedIndex(index0)) {
                    super.removeSelectionInterval(index0, index1);
                } else {
                    super.addSelectionInterval(index0, index1);
                }
            }
        };

        studentsList.setSelectionModel(selectionModel);

        Set<Student> students = Database.get().getStudents();

        for (Student student : students) {
            listModel.addElement(student);
        }

        return studentsList;
    }

    public void showDialog() {
        dialog.setVisible(true);
        dialog.dispose();
    }

    public void addSelectedStudents() {
        mediator.addListOfStudentsToCourse(studentsList.getSelectedValuesList());
        dialog.dispose();
    }

    public void addStudentsFromFile() {
        mediator.addStudentsToCourseFromFile();
        dialog.dispose();
    }

    public void addNewStudent() {
        String[] studentData = GUIManager.get().showNewStudentDialog(dialog);

        if (studentData != null) {
            mediator.addStudentToCourse(studentData);
        }

        dialog.dispose();
    }
}
